package com.example.uberclone2

import android.Manifest
import android.os.Bundle
import android.location.LocationManager
import android.location.LocationListener
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.content.pm.PackageManager
import android.Manifest.permission
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import androidx.core.content.ContextCompat
import androidx.annotation.NonNull
import android.text.method.TextKeyListener.clear
import androidx.core.app.ActivityCompat
import android.os.Build
import android.location.Location
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*

import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.parse.*
import kotlinx.android.synthetic.main.activity_driver_request_list.*
import java.util.ArrayList

class DriverRequestListActivity : AppCompatActivity(), View.OnClickListener,
    AdapterView.OnItemClickListener  {

    private var locationListener: LocationListener? = null
    var fusedLocationProviderClient: FusedLocationProviderClient? = null
    internal lateinit var mLocationRequest: LocationRequest

    internal lateinit var mLocationCallback: LocationCallback
    private var nearByDriveRequests: ArrayList<String>? = null
    private var adapter: ArrayAdapter<*>? = null
    private var passengersLatitudes: ArrayList<Double>? = null
    private var passengersLongitudes: ArrayList<Double>? = null
    private var requestcarUsernames: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_request_list)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        btnGetRequests.setOnClickListener(this)


        nearByDriveRequests = ArrayList()
        passengersLatitudes = ArrayList()
        passengersLongitudes = ArrayList()
        requestcarUsernames = ArrayList()
        adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
            nearByDriveRequests!!
        )

        requestListView.adapter = adapter


        nearByDriveRequests!!.clear()



        if (Build.VERSION.SDK_INT < 23 || ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {

            initializeLocationListener()

        }

        requestListView.setOnItemClickListener(this)


    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.driver_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.getItemId() === R.id.driverLogoutItem) {

            ParseUser.logOutInBackground(object : LogOutCallback {
                override fun done(e: ParseException?) {
                    if (e == null) {

                        finish()

                    }
                }
            })

        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(view: View) {


        if (Build.VERSION.SDK_INT < 23) {

            val locationTask = fusedLocationProviderClient!!.lastLocation

            locationTask.addOnSuccessListener { location ->
                updateRequestsListView(location)
            }

        } else if (Build.VERSION.SDK_INT >= 23) {

            if (ContextCompat.checkSelfPermission(
                    this@DriverRequestListActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {

                ActivityCompat.requestPermissions(
                    this@DriverRequestListActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1000
                )


            } else {

                val locationTask = fusedLocationProviderClient!!.lastLocation

                locationTask.addOnSuccessListener { location ->
                    updateRequestsListView(location)
                }


            }
        }


    }

    private fun updateRequestsListView(driverLocation: Location?) {

        if (driverLocation != null) {

            saveDriverLocationToParse(driverLocation!!)


            val driverCurrentLocation =
                ParseGeoPoint(driverLocation!!.getLatitude(), driverLocation!!.getLongitude())

            val requestCarQuery = ParseQuery.getQuery<ParseObject>("RequestCar")
            requestCarQuery.whereNear("passengerLocation", driverCurrentLocation)
            requestCarQuery.whereDoesNotExist("driverOfMe")
            requestCarQuery.findInBackground(object : FindCallback<ParseObject> {
                override fun done(objects: List<ParseObject>, e: ParseException?) {

                    if (e == null) {
                        if (objects.size > 0) {

                            if (nearByDriveRequests!!.size > 0) {
                                nearByDriveRequests!!.clear()
                            }
                            if (passengersLatitudes!!.size > 0) {
                                passengersLatitudes!!.clear()
                            }
                            if (passengersLongitudes!!.size > 0) {
                                passengersLongitudes!!.clear()
                            }
                            if (requestcarUsernames!!.size > 0) {
                                requestcarUsernames!!.clear()
                            }

                            for (nearRequest in objects) {


                                val pLocation =
                                    nearRequest.get("passengerLocation") as ParseGeoPoint?
                                val milesDistanceToPassenger =
                                    driverCurrentLocation.distanceInMilesTo(pLocation)

                                // 5.87594834787398943 * 10

                                //  58.246789 // Result
                                // 58
                                val roundedDistanceValue =
                                    (Math.round(milesDistanceToPassenger * 10) / 10).toFloat()

                                nearByDriveRequests!!.add(
                                    "There are " + roundedDistanceValue + " miles to " + nearRequest.get(
                                        "username"
                                    )
                                )

                                passengersLatitudes!!.add(pLocation!!.latitude)
                                passengersLongitudes!!.add(pLocation.longitude)
                                requestcarUsernames!!.add(nearRequest.get("username")!!.toString() + "")

                            }

                            adapter!!.notifyDataSetChanged()
                        } else {
                            Toast.makeText(
                                this@DriverRequestListActivity,
                                "Sorry. There are no requests yet",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                        adapter!!.notifyDataSetChanged()


                    }


                }
            })

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 1000 && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(
                    this@DriverRequestListActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {

                initializeLocationListener()


                val locationTask = fusedLocationProviderClient!!.lastLocation

                locationTask.addOnSuccessListener { location ->
                    updateRequestsListView(location)
                }
            }
        }

    }


    override fun onItemClick(
        parent: AdapterView<*>, view: View,
        position: Int, id: Long
    ) {

        //  Toast.makeText(this, "Clicked", Toast.LENGTH_LONG).show();

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {

            val cdLocation =  fusedLocationProviderClient!!.lastLocation

            cdLocation.addOnSuccessListener { location ->
                if (location != null) {
                    val intent = Intent(this, ViewLocationsMapActivity::class.java)
                    intent.putExtra("dLatitude", location.latitude)
                    intent.putExtra("dLongitude", location.longitude)
                    intent.putExtra("pLatitude", passengersLatitudes!![position])
                    intent.putExtra("pLongitude", passengersLongitudes!![position])

                    intent.putExtra("rUsername", requestcarUsernames!![position])
                    startActivity(intent)
                }
            }



        }
    }

    private fun initializeLocationListener() {

        locationListener = object : LocationListener {
            @SuppressLint("MissingPermission")
            override fun onLocationChanged(location: Location) {

                mLocationRequest = LocationRequest()
                mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                mLocationRequest!!.setInterval(10000)
                mLocationRequest!!.setFastestInterval(2000)

                mLocationCallback = LocationCallback()

                LocationServices.getFusedLocationProviderClient(this@DriverRequestListActivity).requestLocationUpdates(
                    mLocationRequest, mLocationCallback,
                    Looper.myLooper()
                )
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {

            }

            override fun onProviderEnabled(provider: String) {

            }

            override fun onProviderDisabled(provider: String) {

            }
        }


    }

    private fun saveDriverLocationToParse(location: Location) {

        val driver = ParseUser.getCurrentUser()
        val driverLocation = ParseGeoPoint(location.getLatitude(), location.getLongitude())
        driver.put("driverLocation", driverLocation)
        driver.saveInBackground(object : SaveCallback {
            override fun done(e: ParseException?) {
                if (e == null) {
                    Toast.makeText(
                        this@DriverRequestListActivity,
                        "Location Saved",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })

    }

}

