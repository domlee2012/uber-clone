package com.example.uberclone2

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.model.LatLngBounds

import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import android.content.Intent.getIntent
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.GoogleMap
import android.content.Intent
import android.net.Uri
import com.google.android.gms.maps.SupportMapFragment
import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.OnMapReadyCallback
import androidx.fragment.app.FragmentActivity
import java.util.ArrayList
import com.parse.*
import kotlinx.android.synthetic.main.activity_view_locations_map.*
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import android.util.Log
import android.widget.Toast


class ViewLocationsMapActivity:FragmentActivity(), OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener {


    private val REQUEST_CODE = 1000
    private var mMap:GoogleMap? = null


    override fun onCreate(savedInstanceState:Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_view_locations_map)
     // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            val mapFragment = supportFragmentManager
    .findFragmentById(R.id.map) as SupportMapFragment?
    mapFragment!!.getMapAsync(this)

        btnRide!!.setText("I want to give " + intent.getStringExtra("rUsername") + " a ride!")

    btnRide!!.setOnClickListener(object:View.OnClickListener {
     override fun onClick(v: View) {

     //Toast.makeText(ViewLocationsMapActivity.this, getIntent().getStringExtra("rUsername"), Toast.LENGTH_LONG).show();

                    val carRequestQuery = ParseQuery.getQuery<ParseObject>("RequestCar")
    carRequestQuery.whereEqualTo("username", intent.getStringExtra("rUsername"))
    carRequestQuery.findInBackground(object:FindCallback<ParseObject> {
     override fun done(objects:List<ParseObject>, e: ParseException?) {

    if (objects.size > 0 && e == null)
    {


    for (uberRequest in objects)
    {


    uberRequest.put("requestAccepted", true)

    uberRequest.put("driverOfMe", ParseUser.getCurrentUser().username)

    uberRequest.saveInBackground(object:SaveCallback {
     override fun done(e:ParseException?) {

    if (e == null)
    {


    val googleIntent = Intent(Intent.ACTION_VIEW,
    Uri.parse(
        "http://maps.google.com/maps?saddr="
        + intent.getDoubleExtra("dLatitude",
        0.0) + ","
        + intent.getDoubleExtra("dLongitude",
        0.0) + "&" + "daddr="
        + intent.getDoubleExtra("pLatitude",
        0.0) + "," +
        intent.getDoubleExtra("pLongitude",
        0.0)
    ))
    startActivity(googleIntent)


        }

     }
    })
    }

    }
    }
    })

    }
    })

    }

    private fun showResponse(view: View?, msg: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Http response")
        builder.setMessage(msg)
        builder.show()
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        if(connectionResult.hasResolution()) {

            try {
                connectionResult.startResolutionForResult(this@ViewLocationsMapActivity, REQUEST_CODE);

            } catch (e : Exception) {

                Log.d("conn", e.getStackTrace().toString());

            }

            } else {

                showResponse(null, "Google play services is not working")
                finish();

            }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
        override fun onMapReady(googleMap:GoogleMap) {
            mMap = googleMap

     // Toast.makeText(this, getIntent().getDoubleExtra("dLatitude", 0) + "" , Toast.LENGTH_LONG).show();



            val dLocation = LatLng(
                intent
    .getDoubleExtra("dLatitude", 0.0),
    intent.getDoubleExtra("dLongitude", 0.0))


            val pLocation = LatLng(intent.getDoubleExtra("pLatitude", 0.0), intent.getDoubleExtra("pLongitude", 0.0))


            val builder = LatLngBounds.Builder()
    val driverMarker = mMap!!.addMarker(MarkerOptions().position(dLocation).title("Driver Location"))
    val passengerMarker = mMap!!.addMarker(MarkerOptions().position(pLocation))

    val myMarkers = ArrayList<Marker>()
    myMarkers.add(driverMarker)
    myMarkers.add(passengerMarker)

    for (marker in myMarkers)
    {

    builder.include(marker.getPosition())

    }

    val bounds = builder.build()
        //screen size for map
        val width = resources.displayMetrics.widthPixels
        val height = resources.displayMetrics.heightPixels
        val padding = (width * 0.12).toInt()
        val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding)
    mMap!!.animateCamera(cameraUpdate)




    }
     }
