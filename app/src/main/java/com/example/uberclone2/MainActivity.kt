package com.example.uberclone2

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.parse.*
import kotlinx.android.synthetic.main.activity_main.*
import com.parse.ParseUser


 class MainActivity : AppCompatActivity() {


    internal enum class State {
        SIGNUP, LOGIN
    }

    private var state: State? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ParseInstallation.getCurrentInstallation().saveInBackground()
        if (ParseUser.getCurrentUser() != null) {

            transitionToPassengerActivity()
            transitionToDriverRequestListActivity()
        }

        state = State.SIGNUP

       
        btnSignUpLogin?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {

                if (state === State.SIGNUP) {

                    if (rdbPassenger.isChecked && rdbDriver.isChecked){
                        Toast.makeText(
                            this@MainActivity,
                            "Are you a driver or a passenger?",
                            Toast.LENGTH_SHORT
                        ).show()
                        return
                    }
                    val appUser = ParseUser()
                    appUser.username = edtUserName?.text.toString()
                    appUser.setPassword(edtPassword?.text.toString())
                    if (rdbDriver.isChecked) {
                        appUser.put("as", "Driver")

                    } else if (rdbPassenger.isChecked) {
                        appUser.put("as", "Passenger")
                    }
                    appUser.signUpInBackground { e ->
                        println(e);
                        if (e == null) {

                            Toast.makeText(this@MainActivity, "Signed Up!", Toast.LENGTH_SHORT)
                                .show()
                            transitionToPassengerActivity()
                            transitionToDriverRequestListActivity()

                        }
                    }

                } else if (state === State.LOGIN) {


                    ParseUser.logInInBackground(
                        edtUserName?.getText().toString(),
                        edtPassword?.getText().toString(),
                        object : LogInCallback {
                            override fun done(user: ParseUser?, e: ParseException?) {

                                if (user != null && e == null) {

                                    Toast.makeText(
                                        this@MainActivity,
                                        "User Logged in",
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    transitionToPassengerActivity()
                                    transitionToDriverRequestListActivity()
                                }
                            }
                        })

                }
            }
        })

        btnForgotPassword.setOnClickListener(this::transitionToForgotPasswordActivity)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.menu_signup_activity, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.getItemId()) {

            R.id.loginItem ->

                if (state === State.SIGNUP) {
                    state = State.LOGIN
                    item.setTitle("Sign Up")
                    btnSignUpLogin?.setText("Log In")
                } else if (state === State.LOGIN) {

                    state = State.SIGNUP
                    item.setTitle("Log In")
                    btnSignUpLogin?.setText("Sign Up")
                }
        }


        return super.onOptionsItemSelected(item)
    }


    fun transitionToDriverRequestListActivity() {
        if (ParseUser.getCurrentUser() != null) {

            if (ParseUser.getCurrentUser().get("as")!!.equals("Driver")) {

                val intent = Intent(this, DriverRequestListActivity::class.java)
                startActivity(intent)

            }

        }
    }

    fun transitionToPassengerActivity(){
        if (ParseUser.getCurrentUser() != null) {

            if (ParseUser.getCurrentUser().get("as") == "Passenger") {

                val intent = Intent(this@MainActivity, PassengerActivity::class.java)
                startActivity(intent)
            }

        }

    }

    private fun transitionToForgotPasswordActivity(v: View) {

            val intent = Intent(v.context, ForgotPasswordActivity::class.java)
            startActivity(intent)

    }

}



