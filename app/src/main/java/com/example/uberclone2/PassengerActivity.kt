package com.example.uberclone2

import android.Manifest
import android.annotation.SuppressLint

import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.*
import com.parse.*
import java.util.*
import kotlin.collections.ArrayList
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.maps.*
import kotlinx.android.synthetic.main.activity_passenger.*
import android.os.Build
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices.*
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationCallback
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient

import com.google.android.gms.tasks.Task


class PassengerActivity : FragmentActivity(), OnMapReadyCallback, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    var locationTask: Task<Location>? = null
    private lateinit var mMap: GoogleMap
    private val REQUEST_CODE = 1000
    var fusedLocationProviderClient: FusedLocationProviderClient? = null
    internal lateinit var mLocationRequest: LocationRequest

    internal lateinit var mLocationCallback: LocationCallback
    private var locationListener: LocationListener? = null

    private var isUberCancelled = true

    private var isCarReady = false
    private var t: Timer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_passenger)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        fusedLocationProviderClient = getFusedLocationProviderClient(this);

        btnRequestCar.setOnClickListener(this)

        btnBeepBeep.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {

                getDriverUpdates()
            }
        })


        val carRequestQuery = ParseQuery.getQuery<ParseObject>("RequestCar")
        carRequestQuery.whereEqualTo("username", ParseUser.getCurrentUser().username)
        carRequestQuery.findInBackground(object : FindCallback<ParseObject> {
            override fun done(objects: List<ParseObject>, e: ParseException?) {
                if (objects.size > 0 && e == null) {

                    isUberCancelled = false
                    btnRequestCar!!.setText("Cancel your uber request!")

                    getDriverUpdates()
                }
            }
        })


        btnLogoutFromPassengerActivity.setOnClickListener {
            ParseUser.logOutInBackground(object : LogOutCallback {
                override fun done(e: ParseException?) {
                    if (e == null) {
                        finish()

                    }
                }
            })
        }

    }




    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap




        locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {

                updateCameraPassengerLocation(location)


            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {

            }

            override fun onProviderEnabled(provider: String) {

            }

            override fun onProviderDisabled(provider: String) {

            }


        }


        if (Build.VERSION.SDK_INT < 23) {

            val locationTask = fusedLocationProviderClient!!.lastLocation

            locationTask.addOnSuccessListener { location ->
                updateCameraPassengerLocation(location)
            }
        } else if (Build.VERSION.SDK_INT >= 23) {

            if (ContextCompat.checkSelfPermission(
                    this@PassengerActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {

                ActivityCompat.requestPermissions(
                    this@PassengerActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1000
                )


            } else {

                mLocationRequest = LocationRequest()
                mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                mLocationRequest!!.setInterval(10000)
                mLocationRequest!!.setFastestInterval(2000)

                mLocationCallback = LocationCallback()

                getFusedLocationProviderClient(this).requestLocationUpdates(
                    mLocationRequest, mLocationCallback,
                    Looper.myLooper()
                )
                locationTask = fusedLocationProviderClient!!.lastLocation

                locationTask!!.addOnSuccessListener { location ->
                    updateCameraPassengerLocation(location)
                }

            }
        }

    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 1000 && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(
                    this@PassengerActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {

                val locationTask = fusedLocationProviderClient!!.lastLocation

                locationTask.addOnSuccessListener { location ->
                    updateCameraPassengerLocation(location)
                }


            }

        }


    }

    private fun updateCameraPassengerLocation(pLocation: Location?) {

        if (!isCarReady) {
            val passengerLocation = LatLng(pLocation!!.latitude, pLocation!!.longitude)
            mMap?.clear()

            mMap!!.addMarker(
                MarkerOptions().position(passengerLocation).title("You are here!!!").icon(
                    BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)
                )
            )

            mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(passengerLocation, 15.0F))


        }
    }

    override fun onClick(view: View) {

        if (!isUberCancelled) {
            val carRequestQuery = ParseQuery.getQuery<ParseObject>("RequestCar")
            carRequestQuery.whereEqualTo("username", ParseUser.getCurrentUser().username)
            carRequestQuery.findInBackground(object : FindCallback<ParseObject> {
                override fun done(requestList: List<ParseObject>, e: ParseException?) {

                    if (requestList.size > 0 && e == null) {

                        isUberCancelled = true
                        btnRequestCar.setText("Request a new uber")

                        for (uberRequest in requestList) {

                            uberRequest.deleteInBackground(object : DeleteCallback {
                                override fun done(e: ParseException?) {

                                    if (e == null) {
                                        showResponse(null, "Request has been deleted")
                                    }
                                }
                            })
                        }

                    }
                }
            })

        } else {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {

                mLocationRequest = LocationRequest()
                mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                mLocationRequest!!.setInterval(10000)
                mLocationRequest!!.setFastestInterval(2000)

                mLocationCallback = LocationCallback()

                getFusedLocationProviderClient(this).requestLocationUpdates(
                    mLocationRequest, mLocationCallback,
                    Looper.myLooper()
                )
                locationTask = fusedLocationProviderClient!!.lastLocation

                locationTask!!.addOnSuccessListener { location ->
                    if (location != null) {

                        val requestCar = ParseObject("RequestCar")
                        requestCar.put("username", ParseUser.getCurrentUser().username)

                        val userLocation = ParseGeoPoint(
                            location.latitude,
                            location.longitude
                        )
                        requestCar.put("passengerLocation", userLocation)

                        requestCar.saveInBackground { e ->
                            if (e == null) {

                                showResponse(null, "Request has been sent")

                                btnRequestCar!!.setText("Cancel your uber order")
                                isUberCancelled = false


                            }
                        }

                    } else {

                        showResponse(null, "Unknown error something went wrong")

                    }
                }
        }
        }


    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        if(connectionResult.hasResolution()) {

            try {
                connectionResult.startResolutionForResult(this@PassengerActivity, REQUEST_CODE);

            } catch (e : Exception) {

                Log.d("conn", e.getStackTrace().toString());

            }

        } else {

            showResponse(null, "Google play services is not working")
            finish();

        }
    }

        private fun showResponse(view: View?, msg: String){
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Http response")
            builder.setMessage(msg)
            builder.show()
        }

        private fun getDriverUpdates() {


            t = Timer()
            t!!.scheduleAtFixedRate(object : TimerTask() {

                override fun run() {

                    val uberRequestQuery = ParseQuery.getQuery<ParseObject>("RequestCar")
                    uberRequestQuery.whereEqualTo("username", ParseUser.getCurrentUser().username)
                    uberRequestQuery.whereEqualTo("requestAccepted", true)
                    uberRequestQuery.whereExists("driverOfMe")

                    uberRequestQuery.findInBackground(object : FindCallback<ParseObject> {
                        override fun done(objects: List<ParseObject>, e: ParseException?) {

                            if (objects.size > 0 && e == null) {

                                isCarReady = true
                                for (requestObject in objects) {

                                    val driverQuery = ParseUser.getQuery()
                                    driverQuery.whereEqualTo(
                                        "username",
                                        requestObject.getString("driverOfMe")
                                    )
                                    driverQuery.findInBackground(object : FindCallback<ParseUser> {
                                        override fun done(drivers: List<ParseUser>, e: ParseException?) {
                                            if (drivers.size > 0 && e == null) {

                                                for (driverOfRequest in drivers) {

                                                    val driverOfRequestLocation =
                                                        driverOfRequest.getParseGeoPoint("driverLocation")
                                                    if (ContextCompat.checkSelfPermission(
                                                            this@PassengerActivity,
                                                            Manifest.permission.ACCESS_FINE_LOCATION
                                                        ) == PackageManager.PERMISSION_GRANTED
                                                    ) {
                                                        val locationTask = fusedLocationProviderClient!!.lastLocation

                                                        locationTask.addOnSuccessListener { location ->
                                                            ParseGeoPoint(
                                                                location!!.latitude,
                                                                location!!.longitude
                                                            )

                                                            val milesDistance =
                                                                driverOfRequestLocation!!.distanceInMilesTo(
                                                                    ParseGeoPoint(
                                                                        location!!.latitude,
                                                                        location!!.longitude
                                                                    )
                                                                )

                                                            if (milesDistance < 0.3) {


                                                                requestObject.deleteInBackground(object :
                                                                    DeleteCallback {
                                                                    override fun done(e: ParseException?) {
                                                                        if (e == null) {

                                                                            showResponse(null, "Your Uber is ready Hurray!")
                                                                            isCarReady = false
                                                                            isUberCancelled = true
                                                                            btnRequestCar!!.setText("You can order a new uber now!")
                                                                        }
                                                                    }
                                                                })

                                                            } else {

                                                                val roundedDistance =
                                                                    (Math.round(milesDistance * 10) / 10).toFloat()
                                                                Toast.makeText(
                                                                    this@PassengerActivity,
                                                                    requestObject.getString("driverOfMe") + " is " + roundedDistance + " miles away from you!- Please wait!!!",
                                                                    Toast.LENGTH_LONG
                                                                ).show()


                                                                val dLocation = LatLng(
                                                                    driverOfRequestLocation.latitude,
                                                                    driverOfRequestLocation.longitude
                                                                )


                                                                val pLocation = LatLng(
                                                                    location.latitude,
                                                                    location.longitude
                                                                )

                                                                val builder = LatLngBounds.Builder()
                                                                val driverMarker = mMap!!.addMarker(
                                                                    MarkerOptions().position(dLocation).title(
                                                                        "Driver Location"
                                                                    )
                                                                )
                                                                val passengerMarker = mMap!!.addMarker(
                                                                    MarkerOptions().position(pLocation)
                                                                )

                                                                val myMarkers = ArrayList<Marker>()
                                                                myMarkers.add(driverMarker)
                                                                myMarkers.add(passengerMarker)

                                                                for (marker in myMarkers) {

                                                                    builder.include(marker.getPosition())

                                                                }

                                                                val bounds = builder.build()

                                                                val cameraUpdate =
                                                                    CameraUpdateFactory.newLatLngBounds(
                                                                        bounds,
                                                                        80
                                                                    )
                                                                mMap!!.animateCamera(cameraUpdate)
                                                            }

                                                        }
                                                        }








                                                }

                                            }
                                        }
                                    })


                                }
                            } else {
                                isCarReady = false
                            }
                        }
                    })

                }

            }, 0, 3000)


    }

}
