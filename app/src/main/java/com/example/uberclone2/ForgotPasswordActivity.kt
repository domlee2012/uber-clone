package com.example.uberclone2

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_forgot_password.*
import com.parse.RequestPasswordResetCallback
import com.parse.ParseUser
import com.parse.ParseException
import androidx.appcompat.app.AlertDialog


class ForgotPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        submitEmail.setOnClickListener(this::submitResetPassword)
    }

    private fun submitResetPassword(v: View){
        ParseUser.requestPasswordResetInBackground(
            txtEmail.text.toString(),
            object : RequestPasswordResetCallback {
                override fun done(e: ParseException?) {
                    if (e == null) {
                        // An email was successfully sent with reset instructions
                        val title = "Password Reset Email Sent! Your password will be the same for driver or passenger"
                        val message = "Check Your Email To Change Your Password"
                        alertDisplayer(title, message);
                    } else {
                        // Something went wrong. Look at the ParseException
                        val title = "Password Reset Failed"
                        val message = "Password cannot be changed"
                        alertDisplayer(title, message);
                    }
                }
            })
    }

    private fun alertDisplayer(title: String, message: String) {
        val builder = AlertDialog.Builder(this@ForgotPasswordActivity)
            .setTitle(title).setMessage(message)
            .setPositiveButton("OK", { dialog, which -> dialog.cancel() })
        val alert = builder.create()
        alert.show()
    }

}